# OpenML dataset: LED-display-domain-7digit

https://www.openml.org/d/40496

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Breiman,L., Friedman,J.H., Olshen,R.A., and Stone,C.J.  
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/LED+Display+Domain), [KEEL](http://sci2s.ugr.es/keel/dataset.php?cod=63, https://archive.ics.uci.edu/ml/datasets/LED+Display+Domain) - 1988  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**LED display data set**  
This simple domain contains 7 Boolean attributes and 10 classes, the set of decimal digits. Recall that LED displays contain 7 light-emitting diodes -- hence the reason for 7 attributes. The class attribute is an integer ranging between 0 and 9 inclusive, representing the possible digits show on the display.

The problem would be easy if not for the introduction of noise.  In this case, each attribute value has the 10% probability of having its value inverted.  

It's valuable to know the optimal Bayes rate for these databases. In this case, the misclassification rate is 26% (74% classification accuracy).
        
### Attribute Information  
* V1-V7 represent each of the 7 LEDs, with values either 0 or 1, according to whether the corresponding light is on or not for the decimal digit. Each has a 10% percent chance of being inverted.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40496) of an [OpenML dataset](https://www.openml.org/d/40496). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40496/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40496/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40496/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

